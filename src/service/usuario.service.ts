import { Injectable } from "@angular/core";
import { Usuario } from "../model/usuario";

import { AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';

@Injectable()
export class UsuarioService
{
  itemsCollection: AngularFirestoreCollection<Usuario> = this.afs.collection<Usuario>('usuario');

  constructor(private afs: AngularFirestore){}

  getUsuario()
  {
    return this.itemsCollection;
  }
  addItem(usuario: Usuario)
  {
    const id = this.afs.createId();
    usuario.key = id;
    this.itemsCollection.doc(id).set(JSON.parse(JSON.stringify(usuario)));

  }

  updateUsuario(usuario:Usuario)
  {
    return this.itemsCollection.doc(usuario.key).update(usuario);
  }

  removeUsuario(usuario:Usuario)
  {
    return this.itemsCollection.doc(usuario.key).delete();
    // return this.itemsCollection.doc(usuario.key).update(usuario);
  }


}
