import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { EditarUsuarioPage } from '../pages/editar-usuario/editar-usuario';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import {UsuarioService} from '../service/usuario.service';

import { AdMobFree } from '@ionic-native/admob-free';

const config = {
  apiKey: "AIzaSyBXzi2_EUtKy262G8BM7yn4F61uFmwdRZM",
  authDomain: "cronometro-ionic.firebaseapp.com",
  databaseURL: "https://cronometro-ionic.firebaseio.com",
  projectId: "cronometro-ionic",
  storageBucket: "cronometro-ionic.appspot.com",
  messagingSenderId: "48534140066"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    EditarUsuarioPage
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule.enablePersistence(),
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    EditarUsuarioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UsuarioService,
    AdMobFree
  ]
})
export class AppModule {}
