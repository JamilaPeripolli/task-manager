import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Usuario } from '../../model/usuario';
import { EditarUsuarioPage } from '../editar-usuario/editar-usuario';
import { UsuarioService } from '../../service/usuario.service';

import { AngularFireDatabase,AngularFireList } from 'angularfire2/database';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private usuario: Usuario = new Usuario();
  items: Observable<Usuario[]>

  //public cronometro = 0;
  //public segundos = 0;
  //public minutos = 0;
  //public hora = 0;
  //public pararCronometro = false;
  constructor(public navCtrl: NavController, private UsuarioService: UsuarioService, private admobFree: AdMobFree) {
    //this.usuario.projeto = "";
    //this.usuario.cronometro = "";
    this.items = this.UsuarioService.getUsuario().valueChanges();
    this.mostrarPublicidade();
  }

  ionViewWillEnter()
  {
    this.usuario.projeto = "";
    this.usuario.cronometro = "";
  }


  startTimer()
  {
    //this.pararCronometro = false;
    var intervalo = setInterval(function(){
      //alert(this.textCronometro++);
      this.segundos++;
      if(this.segundos == 60){
        this.segundos = 0;
        this.minutos++;
        if(this.minutos == 60){
          this.minutos = 0;
          this.hora++;
        }
      }


      this.cronometro = this.hora + ":" + this.minutos + ":" + this.segundos;

    if(this.pararCronometro){
      //return;
      --this.segundos;
      clearInterval(intervalo);
    }
    }.bind(this),1000)
  }

  stopTimer(){
    //this.pararCronometro = true;
  }

  cadastrarUsuario()
  {
    this.UsuarioService.addItem(this.usuario);
  }

  editarRemover(usuarioEditarRemover: Usuario)
  {
    this.navCtrl.push(EditarUsuarioPage, {usuario: usuarioEditarRemover});
  }

  mostrarPublicidade()
  {
    const bannerConfig: AdMobFreeBannerConfig = {
     id: 'ca-app-pub-5861210470814131/3081473737',
     // id: 'ca-app-pub-3940256099942544/6300978111', //teste
     isTesting: false,
     autoShow: true
    };
    this.admobFree.banner.config(bannerConfig);

    this.admobFree.banner.prepare()
      .then(() => {
        // banner Ad is ready
        // if we set autoShow to false, then we will need to call the show method here
      })
      .catch(e => console.log(e));

  }

}
