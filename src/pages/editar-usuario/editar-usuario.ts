import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EditarUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 import { Usuario } from '../../model/usuario';
 import { UsuarioService } from '../../service/usuario.service';

@IonicPage()
@Component({
  selector: 'page-editar-usuario',
  templateUrl: 'editar-usuario.html',
})
export class EditarUsuarioPage {
  private usuario: Usuario = new Usuario();

  constructor(public navCtrl: NavController, public navParams: NavParams
  , public usuarioService:UsuarioService) {
    this.usuario = this.navParams.get('usuario');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditarUsuarioPage');
  }

  alterarUsuario()
  {
    this.usuarioService.updateUsuario(this.usuario);
    this.navCtrl.pop();
  }

  removeUsuario()
  {
    this.usuarioService.removeUsuario(this.usuario);
    this.navCtrl.pop();
  }


}
